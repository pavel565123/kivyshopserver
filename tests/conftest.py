import pytest
from models import Category, Product


@pytest.fixture(scope='module')
def new_category():
    category = Category(id=2, category_name='Fiction')
    return category


@pytest.fixture(scope='module')
def new_product():
    product = Product(id=5, product_name="1984", author_name="George Orwell",
                      product_description="Among the seminal texts of the 20th century...",
                      price=7.89, category_id=1)
    return product
