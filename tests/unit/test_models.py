# from models import Category, Product


def test_new_category(new_category):
    """
    GIVEN a Category model
    WHEN a new Category is created
    THEN check the id and category_name fields are defined correctly
    """
    # category = Category(id=2, category_name='Fiction')
    assert type(new_category.id) == int
    assert new_category.category_name != 'Programming'
    assert new_category.__repr__() == "<Category 'Fiction'>"


def test_new_product(new_product):
    """
    GIVEN a Product model
    WHEN a new Product is created
    THEN check the id, product_name, author_name, product_description, price, category_id fields are defined correctly
    """
    # product = Product(id=5, product_name="1984", author_name="George Orwell",
    #           product_description="Among the seminal texts of the 20th century...",
    #           price=7.89, category_id=1)
    lowest_price = 0.01
    test_category_id = 2

    assert type(new_product.id) == int
    assert new_product.product_name != 'The Lord of The Rings'
    assert type(new_product.author_name) == str
    assert type(new_product.product_description) == str
    assert type(new_product.price) == float
    assert new_product.price >= lowest_price
    assert new_product.category_id != test_category_id
    assert new_product.__repr__() == "<Product '1984'>"
